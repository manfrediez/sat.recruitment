﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using Sat.Recruitment.Api.Business;
using Sat.Recruitment.Api.Dto;
using Sat.Recruitment.Api.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Controllers
{
    public class Result
    {
        public bool IsSuccess { get; set; }
        public string Errors { get; set; }
    }

    [ApiController]
    [Route("[controller]")]
    public partial class UsersController : ControllerBase
    {
        private IMapper _mapper;

        private readonly List<User> _users = new List<User>();
        public UsersController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpPost]
        [Route("/create-user")]
        public async Task<Result> CreateUser(UserDto userDto)
        {

            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

                return new Result()
                {
                    IsSuccess = false,
                    Errors = allErrors.ToString()
                };
            }

            var user = _mapper.Map<User>(userDto);

            try
            {
                await new UserBusiness().CreateUser(user);
            }
            catch (Exception ex)
            {
                return new Result()
                {
                    IsSuccess = false,
                    Errors = ex.Message
                };
            }

            return new Result()
            {
                IsSuccess = true,
                Errors = "User Created"
            };
        }
    }
}
