﻿using Sat.Recruitment.Api.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.DataAcess
{
    public class UserDataAcess
    {
        public async Task<List<User>> GetUsers()
        {
            var path = Directory.GetCurrentDirectory() + "/Files/Users.txt";
            List<User> users = File.ReadAllLines(path)
                                           .Select(v => User.FromCsv(v))
                                           .ToList();

            return users;
        }
    }
}
