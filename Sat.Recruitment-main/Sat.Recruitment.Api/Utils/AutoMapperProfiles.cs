﻿using AutoMapper;
using Sat.Recruitment.Api.Dto;
using Sat.Recruitment.Api.Models;

namespace Sat.Recruitment.Api.Utils
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<UserDto, User>()
                .ForMember(dest => dest.Money, opt => opt.MapFrom(src => decimal.Parse(src.Money))); 
        }
    }
}
