﻿using Sat.Recruitment.Api.DataAcess;
using Sat.Recruitment.Api.Dto;
using Sat.Recruitment.Api.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Business
{
    public class UserBusiness : IUserBusiness
    {
        public async Task CreateUser(User newUser)
        {
            if (newUser.Money > 100 || (newUser.UserType == UserType.Normal && newUser.Money < 100 && newUser.Money > 10))
            {
                decimal percentage = User.GetPercentage(newUser.UserType, newUser.Money);
                newUser.Money = newUser.Money * percentage;
            }

            string emailNormalize = User.EmailNormalize(newUser.Email);

            var users =  await new UserDataAcess().GetUsers();

            if (users.Any(x => (x.Email == newUser.Email || x.Email == emailNormalize || x.Phone == newUser.Phone) || (x.Name == newUser.Name && x.Address == newUser.Address)))
            {
                Debug.WriteLine("The user is duplicated");
                throw new Exception("The user is duplicated");
            }

            Debug.WriteLine("User Created");

        }
    }
}
