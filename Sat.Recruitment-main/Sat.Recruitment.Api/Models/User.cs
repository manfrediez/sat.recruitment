﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sat.Recruitment.Api.Models
{
    public enum UserType
    {
        Normal,
        SuperUser,
        Premium
    }
    public class User
    {
        [Required]
        public string Name { get; set; } = null!;
        [Required]
        public string Email { get; set; } = null!;
        [Required]
        public string Address { get; set; } = null!;
        [Required]
        public string Phone { get; set; } = null!;
        public UserType UserType { get; set; }
        public decimal Money { get; set; }

        public static User FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            var user = new User
            {
                Name = values[0].ToString(),
                Email = values[1].ToString(),
                Phone = values[2].ToString(),
                Address = values[3].ToString(),
                UserType = Enum.Parse<UserType>(values[4].ToString()),
                Money = decimal.Parse(values[5].ToString()),
            };
            return user;
        }

        public static decimal GetPercentage(UserType userType, decimal money)
        {
            switch (userType)
            {
                case UserType.Normal:
                    if (money > 100)
                        return Convert.ToDecimal(1.12);
                    else
                        return Convert.ToDecimal(1.8); 
                case UserType.SuperUser:
                    return Convert.ToDecimal(1.20);
                case UserType.Premium:
                    return 3;
            }

            return 1;
        }

        public static string EmailNormalize(string Email)
        {
            var aux = Email.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
            if (aux.Length > 1)
            {
                var atIndex = aux[0].IndexOf("+", StringComparison.Ordinal);
                aux[0] = atIndex < 0 ? aux[0].Replace(".", "") : aux[0].Replace(".", "").Remove(atIndex);
                return string.Join("@", new string[] { aux[0], aux[1] });
            }
            else return aux[0];
        }
    }

}
