using System;
using System.Dynamic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

using Sat.Recruitment.Api.Controllers;
using Sat.Recruitment.Api.Dto;
using Sat.Recruitment.Api.Models;
using Xunit;

namespace Sat.Recruitment.Test
{
    [CollectionDefinition("Tests", DisableParallelization = true)]
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDto>().ReverseMap());
            var mapper = config.CreateMapper();

            var userController = new UsersController(mapper);
            UserDto user = new UserDto
            {
                Name = "Mike",
                Email = "mike@gmail.com",
                Address = "Av. Juan G",
                Phone = "+349 1122354215",
                UserType = "Normal",
                Money = "124"
            };

            var result = userController.CreateUser(user).Result;

            Assert.True(result.IsSuccess);
            Assert.Equal("User Created", result.Errors);
        }

        [Fact]
        public void Test2()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDto>().ReverseMap());
            var mapper = config.CreateMapper();

            var userController = new UsersController(mapper);

            UserDto user = new UserDto
            {
                Name = "Agustina",
                Email = "Agustina@gmail.com",
                Address = "Av. Juan G",
                Phone = "+349 1122354215",
                UserType = "Normal",
                Money = "124"
            };

            var result = userController.CreateUser(user).Result;

            Assert.True(false, result.Errors);
            Assert.Equal("User Created", result.Errors);
        }
    }
}
